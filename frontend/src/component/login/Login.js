import React, { Component } from 'react';
import Header from '../header/Header';
import './Login.css';
import Axios from 'axios';

export class Login extends Component {
	constructor() {
		super();
		this.state = {
			nama: '',
			password: '',
			errors: {}
		};
    }
    
    

	onSubmit = (e) => {
		e.preventDefault();
		// const LoginRequest = {
		// 	nama: this.state.nama,
		// 	password: this.state.password
		// };

		
	};

	onChange = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};

	login = ()=>{
		Axios.get(`http://localhost:8080/petugass`)
		.then((res)=>{
			const petugas = res.data;
			this.setState({petugas});
		})
		if(this.state.nama==="admin" && this.state.password==="password"){
			this.props.history.push('/listBuku')
		}else {
			alert("username atau password salah")
		}
		
	}

	render() {
		return (
			<div>
				<Header />

				<div>
					<form onSubmit={this.onSubmit} className="container">
						<div className="imgcontainer">
							<img
								src="https://icon-library.com/images/avatar-icon/avatar-icon-8.jpg"
								alt="Avatar"
								className="avatar"
							/>
						</div>

						<div className="container">
							<label >
								<b>Username</b>
							</label>
							<input
								type="text"
								name="nama"
								onChange={this.onChange}
								placeholder="Enter Username"
								required
							/>

							<label>
								<b>Password</b>
							</label>
							<input
								type="password"
								name="password"
								onChange={this.onChange}
								placeholder="Enter Password"
								required
							/>

							<button type="submit" onClick={() => this.login()}>Login</button>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

export default Login;
