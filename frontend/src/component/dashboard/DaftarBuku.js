import React, { Component } from 'react'
import axios from 'axios'

export class DaftarBuku extends Component {
    constructor() {
		super();
		this.state = {
			bukus: [],
		};
    }
    componentDidMount() {
		axios
			.get(`http://localhost:8080/bukus`)
			.then((res) => {
				const bukus = res.data;
				this.setState({ bukus });
			})
			.catch((error) => {
				console.log(error);
			});
	}
    render() {
        return (
            <div>
                <div className="body">
					<h2>Daftar Buku Perpustakaan Universal</h2>
					<div style={{ overflowX: 'auto' }}>
						<table>
							<thead>
								<tr>
									<th>No</th>
									<th>Judul Buku</th>
									<th>Pengarang</th>
									<th>Penerbit</th>
									<th>Tahun Terbit</th>
								</tr>
							</thead>
							<tbody>
								{this.state.bukus.length ? (
									this.state.bukus.map((buku, i) => (
										<tr>
											<td>{i + 1}</td>
											<td>{buku.judul}</td>
											<td>{buku.pengarang}</td>
											<td>{buku.penerbit}</td>
											<td>{buku.tahunTerbit}</td>
										</tr>
									))
								) : (
									<tr />
								)}
								<tr>
									<td />
									<td />
									<td />
									<td />
									<td />
								</tr>
							</tbody>
						</table>
					</div>
				</div>
            </div>
        )
    }
}

export default DaftarBuku
