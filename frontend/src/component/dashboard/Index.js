import React, { Component } from 'react'
import Header from '../header/Header'
import DaftarBuku from './DaftarBuku'
import './Dashboard.css'

export default class Index extends Component {
    render() {
        return (
            <div>
                <Header/>
                <div className="content">
                    <h1>Selamat Datang di Perpustakaan Universal</h1>
                    <h3>Mari Budayakan Membaca Buku dari Sekarang</h3>
                </div>
                <DaftarBuku/>
            </div>
        )
    }
}
