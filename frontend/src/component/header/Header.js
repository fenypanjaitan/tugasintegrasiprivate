import React, { Component } from 'react';
import './Header.css';

export class Header extends Component {
	render() {
		return (
			<div>
				<div className="header">
					<a href="/" className="logo">
						Perpustakaan Universal
					</a>
					<div className="header-right">
						<a href="/">
							Home
						</a>
						<a href="#contact">Contact</a>
						<a href="/login">Petugas</a>
					</div>
				</div>
			</div>
		);
	}
}

export default Header;
