import React, { Component } from 'react'

export class HeaderPetugas extends Component {
    render() {
        return (
            <div>
                <div className="header">
					<a href="/listBuku" className="logo">
						Perpustakaan Universal
					</a>
					<div className="header-right">
						<a href="/listBuku">
							Home
						</a>
						<a href="#contact">Contact</a>
						<a href="#logout">Logout</a>
					</div>
				</div>
            </div>
        )
    }
}

export default HeaderPetugas
