import React, { Component } from 'react';
import axios from 'axios';
import './Buku.css';
import { Link } from 'react-router-dom';
import HeaderPetugas from '../header/HeaderPetugas';
export class ListBuku extends Component {
	constructor() {
		super();
		this.state = {
			bukus: [],
			id: 0
		};
	}
	componentDidMount() {
		axios
			.get(`http://localhost:8080/bukus`)
			.then((res) => {
				const bukus = res.data;
				this.setState({ bukus });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	delete = (id) => {
		axios
			.delete('http://localhost:8080/delete/' + id)
			.then(() => {
				window.location.reload();
			})
			.catch((error) => {
				console.log(error);
			});
	};

	edit = (id) => {
		console.log(id);
		this.setState({
			id
		});
	};

	render() {
		console.log('kkkkkk ' + this.state.id);
		return (
			<div>
				<HeaderPetugas />
				<div className="body">
					<Link to="/addBuku">
						<button className="btnAdd">Tambah Buku</button>
					</Link>
					<h2>Daftar Buku Perpustakaan Universal</h2>
					<div style={{ overflowX: 'auto' }}>
						<table>
							<thead>
								<tr>
									<th>No</th>
									<th>Judul Buku</th>
									<th>Pengarang</th>
									<th>Penerbit</th>
									<th>Tahun Terbit</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								{this.state.bukus.length ? (
									this.state.bukus.map((buku, i) => (
										<tr>
											<td>{i + 1}</td>
											<td>{buku.judul}</td>
											<td>{buku.pengarang}</td>
											<td>{buku.penerbit}</td>
											<td>{buku.tahunTerbit}</td>
											<td>
												{/* <button className="btnEdit" onClick={() => {this.edit(buku.id)}}>Edit</button> */}
												<Link to={'/editBuku/' + buku.id}>
													<button
														className="btnEdit"
														buku={this.state.bukus}
														key={this.state.bukus.id}
													>
														Edit
													</button>{' '}
												</Link>
												<button
													className="btnDelete"
													onClick={() => {
														if (window.confirm('Yakin ingin menghapus data?')) {
															this.delete(buku.id);
														}
													}}
												>
													Hapus
												</button>
											</td>
										</tr>
									))
								) : (
									<tr />
								)}
								<tr>
									<td />
									<td />
									<td />
									<td />
									<td />
									<td />
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
}

export default ListBuku;
