import React, { Component } from 'react';
import axios from 'axios';
import './Buku.css';
import HeaderPetugas from '../header/HeaderPetugas';

export class AddBuku extends Component {
	constructor(props) {
		super(props);
		this.state = {
			judul: '',
			pengarang: '',
			penerbit: '',
			tahunTerbit: ''
		};
	}
	handleChange = (event) => {
		this.setState({ [event.target.name]: event.target.value });
	};

	handleSubmit = (tambah) => {
		tambah.preventDefault();
        axios.post('http://localhost:8080/addBuku', this.state)
        this.props.history.push('/listBuku')
        window.location.reload()
	};
	render() {
		return (
			<div>
				<HeaderPetugas />
				<div className="container">
					<h2>Tambahkan Buku Baru</h2>
					<form onSubmit={this.handleSubmit}>
						<div className="row">
							<div className="col-25">
								<label for="judul">Judul Buku</label>
							</div>
							<div className="col-75">
								<input
									type="text"
									name="judul"
									onChange={this.handleChange}
									placeholder="Judul buku.."
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-25">
								<label for="pengarang">Pengarang</label>
							</div>
							<div className="col-75">
								<input
									type="text"
									name="pengarang"
									onChange={this.handleChange}
									placeholder="Nama Pengarang.."
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-25">
								<label for="penerbit">Penerbit</label>
							</div>
							<div className="col-75">
								<input
									type="text"
									name="penerbit"
									onChange={this.handleChange}
									placeholder="Penerbit.."
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-25">
								<label for="tahunTerbit">Tahun Terbit</label>
							</div>
							<div className="col-75">
								<input
									type="number"
									name="tahunTerbit"
									onChange={this.handleChange}
									placeholder="Tahun Terbit.."
								/>
							</div>
						</div>
						<div className="row" style={{ marginTop: 20 }}>
							<input type="submit" value="Tambah" />
						</div>
					</form>
				</div>
			</div>
		);
	}
}

export default AddBuku;
