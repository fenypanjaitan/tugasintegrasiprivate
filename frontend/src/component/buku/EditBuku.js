import React, { Component } from 'react'
import axios from 'axios';
import HeaderPetugas from '../header/HeaderPetugas';


export class EditBuku extends Component {
    constructor(props) {
		super(props);
		this.state = {
            id:'',
			judul: '',
			pengarang: '',
			penerbit: '',
			tahunTerbit: ''
		};
    }
    handleChange = (event) => {
		this.setState({ [event.target.name]: event.target.value });
    };
    
    componentDidMount() {
        const id = this.props.match.params.id
        console.log("pppppppppp "+this.props.match.params);
		axios
			.get(`http://localhost:8080/bukus/`+id)
			.then((res) => {
                this.setState({
                    id: res.data.id,
                    judul: res.data.judul,
                    pengarang: res.data.pengarang,
                    penerbit: res.data.penerbit,
                    tahunTerbit: res.data.tahunTerbit
                })
			})
			.catch((error) => {
				console.log(error);
			});
    }

   

    handlerSubmit = async (e) => {
        e.preventDefault()
        await axios.put('http://localhost:8080/update/' + this.state.id, this.state)
        this.props.history.push('/listBuku')
    }
    
    render() {
        const { judul, pengarang, penerbit, tahunTerbit } = this.state
        console.log(">>>>>>>>>>>>"+{judul});
        return (
            <div>
                <HeaderPetugas />
				<div className="container">
					<h2>Edit Buku</h2>
					<form >
						<div className="row">
							<div className="col-25">
								<label>Judul Buku</label>
							</div>
							<div className="col-75">
								<input
									type="text"
									name="judul"
                                    onChange={this.handleChange}
                                    value={judul}
									placeholder="Judul buku.."
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-25">
								<label >Pengarang</label>
							</div>
							<div className="col-75">
								<input
									type="text"
                                    name="pengarang"
                                    value={pengarang}
									onChange={this.handleChange}
									placeholder="Nama Pengarang.."
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-25">
								<label >Penerbit</label>
							</div>
							<div className="col-75">
								<input
									type="text"
                                    name="penerbit"
                                    value={penerbit}
									onChange={this.handleChange}
									placeholder="Penerbit.."
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-25">
								<label>Tahun Terbit</label>
							</div>
							<div className="col-75">
								<input
									type="number"
                                    name="tahunTerbit"
                                    value={tahunTerbit}
									onChange={this.handleChange}
									placeholder="Tahun Terbit.."
								/>
							</div>
						</div>
						<div className="row" style={{ marginTop: 20 }}>
							<input type="submit" value="Edit" onClick={this.handlerSubmit} style={{backgroundColor:"#f6c609"}}/>
						</div>
					</form>
				</div>
            </div>
        )
    }
}

export default EditBuku
