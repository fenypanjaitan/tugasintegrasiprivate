import React from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom';
import Index from './component/dashboard/Index';
import ListBuku from './component/buku/ListBuku.js';
import AddBuku from './component/buku/AddBuku';
import EditBuku from './component/buku/EditBuku';
import Login from './component/login/Login';

function App() {
  return (
    <Switch>
		<Route path="/" exact component={Index} />
		<Route path="/login" exact component={Login} />
		<Route path="/listBuku" component={ListBuku}/>
		<Route path="/addBuku" component={AddBuku}/>
		<Route path="/editBuku/:id" component={EditBuku}/>
	</Switch>
    
  );
}

export default App;
