package com.example.demo.repository;

import com.example.demo.entity.PetugasPerpus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetugasPerpusRepository extends JpaRepository<PetugasPerpus, Integer> {
    PetugasPerpus findByNama(String nama);
}
