package com.example.demo.service;

import com.example.demo.entity.PetugasPerpus;
import com.example.demo.repository.PetugasPerpusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PetugasPerpusService {
    @Autowired
    private PetugasPerpusRepository petugasRepository;

    public PetugasPerpus savePetugas(PetugasPerpus petugas){
        return (PetugasPerpus) petugasRepository.save(petugas);
    }

    public List<PetugasPerpus> getPetugass(){
        return petugasRepository.findAll();
    }

    public PetugasPerpus getPetugasById(int id){
        return petugasRepository.findById(id).orElse(null);
    }

    public PetugasPerpus getPetugasByNama(String nama){
        return petugasRepository.findByNama(nama);
    }

    public String deletePetugas(int id){
        petugasRepository.deleteById(id);
        return "Petugas dihapus";
    }

    public PetugasPerpus updatePetugas(PetugasPerpus petugas){
        PetugasPerpus existingPetugas = (PetugasPerpus) petugasRepository.findById(petugas.getId()).orElse(null);
        existingPetugas.setNama(petugas.getNama());
        existingPetugas.setPassword(petugas.getPassword());

        return (PetugasPerpus) petugasRepository.save(existingPetugas);
    }
}
