package com.example.demo.service;

import com.example.demo.entity.Member;
import com.example.demo.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberService {
    @Autowired
    private MemberRepository memberRepository;

    public Member saveMember(Member member){
        return (Member) memberRepository.save(member);
    }

    public List<Member> getMembers(){
        return memberRepository.findAll();
    }

    public Member getMemberById(int id){
        return memberRepository.findById(id).orElse(null);
    }

    public Member getMemberByNama(String nama){
        return memberRepository.findByNama(nama);
    }


    public String deleteMember(int id){
        memberRepository.deleteById(id);
        return "Member dihapus";
    }

    public Member updateMember(Member member){
        Member existingMember = (Member) memberRepository.findById(member.getId()).orElse(null);
        existingMember.setNama(member.getNama());
        existingMember.setAlamat(member.getAlamat());
        existingMember.setNoHp(member.getNoHp());

        return (Member) memberRepository.save(existingMember);
    }
}

