package com.example.demo.service;

import com.example.demo.entity.Buku;
import com.example.demo.repository.BukuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BukuService {
    @Autowired
    private BukuRepository repository;

    public Buku saveBuku(Buku buku){
        return (Buku) repository.save(buku);
    }

    public List<Buku> getBukus(){
        return repository.findAll();
    }

    public Buku getBukuById(int id){
        return repository.findById(id).orElse(null);
    }

    public Buku getBukuByJudul(String judul){
        return repository.findByJudul(judul);
    }

    public Buku getBukuByPengarang(String pengarang){
        return repository.findByPengarang(pengarang);
    }

    public String deleteBuku(int id){
        repository.deleteById(id);
        return "Buku dihapus";
    }

    public Buku updateBuku(Buku buku){
        Buku existingBuku = (Buku) repository.findById(buku.getId()).orElse(null);
        existingBuku.setJudul(buku.getJudul());
        existingBuku.setPengarang(buku.getPengarang());
        existingBuku.setPenerbit(buku.getPenerbit());
        existingBuku.setTahunTerbit(buku.getTahunTerbit());

        return (Buku) repository.save(existingBuku);
    }

}
