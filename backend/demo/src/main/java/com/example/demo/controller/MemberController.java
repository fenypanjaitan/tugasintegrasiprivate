package com.example.demo.controller;

import com.example.demo.entity.Member;
import com.example.demo.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class MemberController {
    @Autowired
    private MemberService memberService;

    @PostMapping("/addMember")
    public Member addMember(@RequestBody Member member){
        return memberService.saveMember(member);
    }

    @GetMapping("/members")
    public List<Member> findAllMembers(){
        return memberService.getMembers();
    }

    @GetMapping("/members/{id}")
    public Member findMemberById(@PathVariable int id){
        return memberService.getMemberById(id);
    }

    @GetMapping("/member/{nama}")
    public Member findMemberByNama(@PathVariable String nama){
        return memberService.getMemberByNama(nama);
    }


    @PutMapping("/updateMember/{id}")
    public Member updateMember(@RequestBody Member member, @PathVariable int id){
        member.setId(id);
        return memberService.updateMember(member);
    }

    @DeleteMapping("/deleteMember/{id}")
    public String deleteMember(@PathVariable int id){
        return memberService.deleteMember(id);
    }

}
