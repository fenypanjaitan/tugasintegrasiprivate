package com.example.demo.controller;

import com.example.demo.entity.Buku;
import com.example.demo.service.BukuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class BukuController {
    @Autowired
    private BukuService service;

    @PostMapping("/addBuku")
    public Buku addBuku(@RequestBody Buku buku){
        return service.saveBuku(buku);
    }

    @GetMapping("/bukus")
    public List<Buku> findAllBukus(){
        return service.getBukus();
    }

//    @PostMapping("/addBukus")
//    public List addBukus(@RequestBody List<Buku> bukus){
//        return service.saveBukus(bukus);
//    }


    @GetMapping("/bukus/{id}")
    public Buku findBukuById(@PathVariable int id){
        return service.getBukuById(id);
    }

    @GetMapping("/buku/{judul}")
    public Buku findBukuByJudul(@PathVariable String judul){
        return service.getBukuByJudul(judul);
    }

    @GetMapping("/bukuByPengarang/{pengarang}")
    public Buku findBukuByPengarang(@PathVariable String pengarang){
        return service.getBukuByPengarang(pengarang);
    }

//    @PutMapping("/update/{id}")
//    public void updateBuku(@RequestBody Buku buku, @PathVariable int id){
//        buku.setId(id);
//        service.updateBuku(buku);
//    }

    @PutMapping("/update/{id}")
    public Buku updateBuku(@RequestBody Buku buku, @PathVariable int id){
        buku.setId(id);
        return service.updateBuku(buku);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteBuku(@PathVariable int id){
        return service.deleteBuku(id);
    }

}
