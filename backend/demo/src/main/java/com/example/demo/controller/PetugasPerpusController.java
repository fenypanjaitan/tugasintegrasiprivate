package com.example.demo.controller;

import com.example.demo.entity.PetugasPerpus;
import com.example.demo.service.PetugasPerpusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class PetugasPerpusController {

    @Autowired
    private PetugasPerpusService petugasService;

    @PostMapping("/addPetugas")
    public PetugasPerpus addPetugas(@RequestBody PetugasPerpus petugas){
        return petugasService.savePetugas(petugas);
    }

    @GetMapping("/petugass")
    public List<PetugasPerpus> findAllPetugass(){
        return petugasService.getPetugass();
    }

    @GetMapping("/petugass/{id}")
    public PetugasPerpus findPetugasById(@PathVariable int id){
        return petugasService.getPetugasById(id);
    }

    @GetMapping("/petugasNama/{nama}")
    public PetugasPerpus findPetugasByNama(@PathVariable String nama){
        return petugasService.getPetugasByNama(nama);
    }

    @PutMapping("/updatePetugas/{id}")
    public PetugasPerpus updatePetugas(@RequestBody PetugasPerpus petugas, @PathVariable int id){
        petugas.setId(id);
        return petugasService.updatePetugas(petugas);
    }

    @DeleteMapping("/deletePetugas/{id}")
    public String deletePetugas(@PathVariable int id){
        return petugasService.deletePetugas(id);
    }

}
