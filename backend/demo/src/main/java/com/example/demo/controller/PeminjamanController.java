package com.example.demo.controller;

import com.example.demo.entity.Peminjaman;
import com.example.demo.service.PeminjamanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class PeminjamanController {
    @Autowired
    private PeminjamanService peminjamanService;

    @PostMapping("/addPeminjaman")
    public Peminjaman addPeminjaman(@RequestBody Peminjaman peminjaman){
        return peminjamanService.savePeminjaman(peminjaman);
    }

    @GetMapping("/peminjamans")
    public List<Peminjaman> findAllPeminjamans(){
        return peminjamanService.getPeminjamans();
    }

    @GetMapping("/peminjamans/{id}")
    public Peminjaman findPeminjamanById(@PathVariable int id){
        return peminjamanService.getPeminjamanById(id);
    }

    @GetMapping("/peminjaman/{tglPinjam}")
    public Peminjaman findPeminjamanByNama(@PathVariable String tglPinjam){
        return peminjamanService.getPeminjamanByTglPinjam(tglPinjam);
    }

    @PutMapping("/updatePeminjaman/{id}")
    public Peminjaman updatePeminjaman(@RequestBody Peminjaman peminjaman, @PathVariable int id){
        peminjaman.setId(id);
        return peminjamanService.updatePeminjaman(peminjaman);
    }

    @DeleteMapping("/deletePeminjaman/{id}")
    public String deletePeminjaman(@PathVariable int id){
        return peminjamanService.deletePeminjaman(id);
    }

}

