package com.example.demo.service;

import com.example.demo.entity.Peminjaman;
import com.example.demo.repository.PeminjamanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeminjamanService {
    @Autowired
    private PeminjamanRepository peminjamanRepository;

    public Peminjaman savePeminjaman(Peminjaman peminjaman){
        return (Peminjaman) peminjamanRepository.save(peminjaman);
    }

    public List<Peminjaman> getPeminjamans(){
        return peminjamanRepository.findAll();
    }

    public Peminjaman getPeminjamanById(int id){
        return peminjamanRepository.findById(id).orElse(null);
    }

    public Peminjaman getPeminjamanByTglPinjam(String tglPinjam) {
        return peminjamanRepository.findByTglPinjam(tglPinjam);
    }

    public String deletePeminjaman(int id){
        peminjamanRepository.deleteById(id);
        return "Peminjaman dihapus";
    }

    public Peminjaman updatePeminjaman(Peminjaman peminjaman){
        Peminjaman existingPeminjaman = (Peminjaman) peminjamanRepository.findById(peminjaman.getId()).orElse(null);
        existingPeminjaman.setTglPinjam(peminjaman.getTglPinjam());
        existingPeminjaman.setTglPengembalian(peminjaman.getTglPengembalian());
        existingPeminjaman.setMember(peminjaman.getMember());
        existingPeminjaman.setBuku(peminjaman.getBuku());

        return (Peminjaman) peminjamanRepository.save(existingPeminjaman);
    }
}
